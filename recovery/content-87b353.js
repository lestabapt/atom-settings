/*! shft.cl - 1.5.x
* https://shft.cl/
* https://deuxhuithuit.com/
* Copyright (c) 2015-2018 Deux Huit Huit (https://deuxhuithuit.com/);
* Licensed Private. All rights reserved.
* DO NOT COPY, NOR EDIT, NOR DISTRIBUTE, NOR GIVE, NOR SALE THIS FILE.
* Use at your own risk.
* Please enjoy responsibly.
*/
(function () {
	'use strict';

	var manifest = chrome.runtime.getManifest();

	var isDev = !('update_url' in manifest);

	var debug = isDev;

	var alwaysHttps = true;

	var server = !isDev ? 'https://shft.cl/' : ((alwaysHttps ? 'https:' : window.location.protocol) + '//localhost:3000/');

	var instanceId = ~~(Math.random() * 10000000);

	var loading = (function (d) {
		var e = d.createElement('div');
		e.innerText = 'Shift Click found the image! Waiting for it...';
		if (!e.style) {
			return e;
		}
		e.style.display = 'block';
		e.style.backgroundColor = 'rgba(255, 255, 255, 0.6)';
		e.style.color = '#333333';
		e.style.fontFamily = 'Arial, Helvetica, sans-serif';
		e.style.position = 'fixed';
		e.style.top = e.style.right = e.style.left = 0;
		e.style.height = e.style.lineHeight = '40px';
		e.style.fontSize = '20px';
		e.style.verticalAlign = 'middle';
		e.style.textAlign = 'center';
		e.style.bottom = 'auto';
		e.style.cursor = 'wait';
		e.style.pointerEvents = 'none';
		e.style.margin = '0';
		e.style.padding = '0';
		e.style.textDecoration = 'none';
		e.style.border = 'none';
		e.style.opacity = 1;
		e.style.zIndex = (Math.pow(2, 31) - 1)|0;
		return e;
	})(document);

	var canvas = (function (d) {
		var e = d.createElement('canvas');
		var init = function () {
			if (!e.style) {
				return;
			}
			e.style.display = 'block';
			e.style.backgroundColor = 'transparent';
			e.style.position = 'fixed';
			e.style.top = e.style.right = e.style.bottom = e.style.left = 0;
			e.style.border = 'none';
			e.style.cursor = 'crosshair';
			e.style.opacity = 1;
			e.style.zIndex = (Math.pow(2, 31) - 2)|0;
		};
		var ctx = e.getContext && e.getContext('2d');
		var pos = {
			x1: 0,
			x2: 0,
			y1: 0,
			y2: 0
		};
		init();
		return {
			elem: e,
			ctx: ctx,
			x: 0,
			y: 0,
			visible: false,
			mousedown: false,
			selection: false,
			supported: !!ctx,
			pos: function () {
				return extend(pos);
			},
			clear: function () {
				ctx.clearRect(0, 0, canvas.width, canvas.height);
			},
			update: function (x, y) {
				pos.x1 = Math.min(canvas.x, x);
				pos.x2 = Math.max(canvas.x, x);
				pos.y1 = Math.min(canvas.y, y);
				pos.y2 = Math.max(canvas.y, y);
			},
			rect: function (options) {
				ctx.fillStyle = 'rgba(0, 255, 0, ' + options.opacity + ')';
				ctx.fillRect(
					pos.x1,
					pos.y1,
					pos.x2 - pos.x1,
					pos.y2 - pos.y1
				);
				ctx.strokeStyle = '#000000';
				ctx.strokeWidth = 2;
				ctx.strokeRect(
					pos.x1,
					pos.y1,
					pos.x2 - pos.x1,
					pos.y2 - pos.y1
				);
			}
		};
	})(document);

	var now = function () {
		return window.performance.now();
	};

	var z = function (z) {
		return parseInt(z, 10) || 0;
	};

	var styleCache;
	var initStyleCache = function () {
		if (!!styleCache && !!styleCache.clear) {
			styleCache.clear();
			styleCache = undefined;
		}
		styleCache = new WeakMap();
	};
	
	var getComputedStyleFor = function (elem) {
		elem = elem.elem || elem;
		if (styleCache.has(elem)) {
			return styleCache.get(elem);
		}
		var s = window.getComputedStyle(elem);
		styleCache.set(elem, s);
		return s;
	};

	var getBoundsFor = function (elem) {
		var bounds = elem.bounds || elem.getBoundingClientRect();
		return {
			top: bounds.top + window.scrollY,
			right: bounds.right + window.scrollX,
			bottom: bounds.bottom + window.scrollY,
			left: bounds.left + window.scrollX,
			width: bounds.width,
			height: bounds.height
		};
	};

	var log = function (msg) {
		if (debug) {
			console.log('[shft.cl]', '(' + instanceId.toString().padStart(7,0) + ')', msg);
		}
	};
	
	var extend = function(obj) {
		var c = {};
		Object.keys(obj).forEach(function (key) {
			c[key] = obj[key];
		});
		return c;
	};

	var urlIsValid = function (url) {
		return !!url &&
			/^.*[\/]{1}.*$/i.test(url) || // schema-less url
			/^https?:[\/]{2}[^\/].*$/i.test(url) || // http/https
			/^data:.+?\/.+?;.+?,.+?$/i.test(url); // data-uri
	};

	var pathIsRelative = function (url) {
		return !(/^https?/i.test(url) || // http/https
				/^data:/i.test(url) ||
				/^s?ftpe?s?:/i.test(url) ||
				/^.*[\/]{1}.*$/i.test(url)
		);
	};
	
	var validateUrl = function (url) {
		var valid = urlIsValid(url);
		if (!valid) {
			return false;
		}
		var schema = url.split(':')[0];
		if (!valid) {
			var issueUrl = 'https://shft.cl/issue';
			if (confirm('Image url schema (' + schema + ') is not valid.\n' +
				'Do you want to report it? \n')) {
				window.location = issueUrl;
			}
		}
		return valid;
	};
	var computeUrlFromPath = function (path) {
		var baseUrl;
		if (/^https?/i.test(path)) {
			return path;
		}
		else if ( !/^[\/]/.test(path)) {
			baseUrl = window.location.href;
			return baseUrl + '/' + path;
		} else {
			baseUrl = window.location.origin;
			return baseUrl + '/' + path;
		}
	};
	var whitespaces = /[\s\n\r\t]{2,}/i;
	
	var splitSrcSet = function (srcset) {
		while (whitespaces.test(srcset)) {
			srcset = srcset.replace(whitespaces, ' ');
		}
		srcset = srcset.replace(/[\s\n\r\t]+$/, '');
		srcset = srcset.replace(/^[\s\n\r\t]+/, '');
		srcset = srcset.split(/,[\s\n\r\t]+/);
		
		return srcset;
	};
	var extractUrlFromOneSrcSet = function (srcset) {
		var paths = splitSrcSet(srcset);
		var l = paths.length - 1 || 0;

		var extractUrlAndSize = function (s) {
			s = s.split(' ');
			var size = s[1];
			var path = s[0];
			size = size.slice(0, -1);
			var url = computeUrlFromPath(path);
			return ({
				size: size,
				url: url
			});
		};
		if (!l) {
			if (/\s/g.test(paths)) {
				paths = paths[0].split(' ');
			}
			return computeUrlFromPath(paths[0]);
		}

		var urlsAndSizes = map(paths, function (path) {
			return extractUrlAndSize(path);
		});
		while (l > 0) {
			if (!urlIsValid(urlsAndSizes[l].url)) {
				urlsAndSizes.splice(l, 1);
			}
			l--;
		}
		if (urlsAndSizes.length <= 0) {
			return;
		}
		var sizes = map(urlsAndSizes, function (urlAndSize) {
			return urlAndSize.size;
		});
		var i = indexOfMax(sizes);

		return urlsAndSizes[i].url;
	};
	var chooseBestUrlFromDifferentSrcSet = function (urls) {
		var minWidthSrcSet = [], maxWidthSrcSet = [], widthSrcSet = [];

		var extractNumberResolution = function (media, strat) {
			return media.match(new RegExp("^.*"+strat+".*?([0-9]+).*"))[1];
		};

		each(urls, function (url) {
			var media = url[1];
			if( /min/i.test(media)) {
				minWidthSrcSet.push([url[0], extractNumberResolution(media, 'min')]);
			} else if ( /max/i.test(media)) {
				maxWidthSrcSet.push([url[0], extractNumberResolution(media, 'max')]);
			}
		});

		if (minWidthSrcSet.length >=1 ) {
			var sizes = map(minWidthSrcSet, function (srcset) {
				return srcset[1];
			});
			var i = indexOfMax(sizes);
			return minWidthSrcSet[i][0];
		} else {
			return urls[0][0];
		}
	};
	var extractAllSourcesAndSizes = function (sources) {
		var urls = [];
		if(sources <= 1) {
			urls = extractUrlFromOneSrcSet(sources[1].srcset) || sources[1].src;
		} else {
			each(sources, function (source) {
				if (!!source.srcset) {
					urls.push([extractUrlFromOneSrcSet(source.srcset), source.media || source.sizes || source.width || 1]);
				} else if (!!source.src) {
					urls.push([source.src, source.media || source.sizes || source.width || 1]);
				}
			});
		}
		return urls;
	};

	var identity = function (i) {
		return i;
	};
	var each = function (a, cb) {
		return Array.prototype.forEach.call(a, cb);
	};
	var map = function (a, cb) {
		var stack = [];
		each(a, function (i, j) {
			stack.push(cb(i, j));
		});
		return stack;
	};
	function indexOfMax(arr) {
		if (arr.length === 0) {
			return -1;
		}
		var max = arr[0];
		var maxIndex = 0;
		for (var i = 1; i < arr.length; i++) {
			if (parseInt(arr[i]) > parseInt(max)) {
				maxIndex = i;
				max = arr[i];
			}
		}
		return maxIndex;
	};
	var arrayize = function (a) {
		return map(a, identity);
	};
	var all = function (target, sel, cb) {
		if (!target) {
			target = document;
		}
		var stack = target.querySelectorAll(sel);
		if (!cb) {
			return stack;
		}
		return each(stack, cb);
		all(elem, 'source');
	};
	var $ = function (sel, cb) {
		return all(document, sel, cb);
	};
	$.remove = function (elem) {
		return elem.parentNode.removeChild(elem);
	};
	$.ajax = function (options) {
		options = options || false;
		var xhr = new XMLHttpRequest();
		xhr.onreadystatechange = function (e) {
			if (xhr.readyState === xhr.UNSENT) {
				options.error && options.error(xhr);
				options.always && options.always(xhr);
				log('Connection to ' + options.url + ' refused.');
				xhr.onreadystatechange = null;
				xhr = null;
				return;
			} else if (xhr.readyState === xhr.OPENED) {
				options.opened && options.opened(xhr);
			} else if (xhr.readyState === xhr.HEADERS_RECEIVED) {
				options.headersReceived && options.headersReceived(xhr);
			} else if (xhr.readyState === xhr.LOADING) {
				options.loading && options.loading(xhr);
			} else if (xhr.readyState === xhr.DONE) {
				if (xhr.status !== 200 && xhr.status !== 201) {
					log('HTTP response != 200: ' + xhr.status);
				} else {
					log('HTTP response == ' + xhr.status + ' no problemo');
				}
				options.completed && options.completed(xhr);
				options.always && options.always(xhr);
				xhr.onreadystatechange = null;
				xhr = null;
			}
		};
		xhr.open(options.verb || 'GET', options.url, true);
		if (options.mime) {
			xhr.setRequestHeader('Content-Type', options.mime);
		}
		if (options.payload && typeof options.payload === 'object') {
			options.payload = JSON.stringify(options.payload);
		}
		xhr.send(options.payload);
	};
	$.ajax.json = function (xhr) {
		var data = {};
		if (xhr.status === 429) {
			data.error = 'too-many-requests?retry-after=' + xhr.getResponseHeader('retry-after');
		} else if (!xhr.responseText) {
			data.error = 'empty-response';
		} else {
			try {
				data = JSON.parse(xhr.responseText);
			}
			catch (ex) {
				data.error = 'invalid-json?msg=' + ex.message;
			}
		}
		return data;
	};

	var storage = chrome.storage.local; // sync
	var user;
	var ping = function () {
		storage.get('ping', function (items) {
			var lastPing = items.ping || 0;
			if (lastPing < Date.now() - (24 * 60 * 60 * 1000)) {
				if (isDev) {
					log('ping in dev aborted!');
					return;
				}
				$.ajax({
					verb: 'POST',
					payload: {
						user: user
					},
					url: server + 'ping',
					mime: 'application/json',
					completed: function (xhr) {
						if (xhr.status === 200) {
							storage.set({ ping: Date.now() }, function () {
								if (chrome.runtime.lastError) {
									log('error while saving data: ' + chrome.runtime.lastError);
								} else if (user) {
									log('ping for user ' + user.id)
								} else {
									log('ping no user');
								}
							});
						}
					}
				});
			}
		});
	};
	
	var posIsInElement = function (pos, elem) {
		var elemPos = getBoundsFor(elem);
		return elemPos.left <= pos.x && elemPos.right >= pos.x &&
			elemPos.top <= pos.y && elemPos.bottom >= pos.y;
	};
	
	var isVisible = function (elem) {
		var visible = true;
		var currentElement = elem;
		while (!!currentElement && currentElement !== document && !!visible) {
			var style = getComputedStyleFor(currentElement);
			if (style.display === 'none' || parseFloat(style.opacity) < 0.01 ||
				style.visibility === 'hidden' || style.visibility === 'collapse') {
				visible = false;
			}
			currentElement = currentElement.parentElement;
		}
		return visible;
	};
	
	var pendingImagesCount = 0;
	var virtualTabId = 0;
	var tabs = {};
	var pendingCloseTabs = {};
	var pendingRedirectionTabs = {};
	var formatSrcForConsole = function (src) {
		if (/^data:/.test(src)) {
			return src.substring(0, 64);
		}
		return src;
	};
	var imageFound = function (img, infos) {
		var src = img.src;
		if (!!img.srcset) {
			src = extractUrlFromOneSrcSet(img.srcset) || src;
		}
		if (pathIsRelative(src)) {
			var baseUrl = window.location.href;
			src =  baseUrl +'/'+src;
		}
		if (!validateUrl(src)) {
			log('Url + ' + src + ' is not valid. Send aborted');
			return false;
		}
		if (window.navigator.onLine === false) {
			alert('You do not seem to be connected to internet...\nShift Click needs internet access');
			return false;
		}
		log('------------------------------');
		log('requesting ' + formatSrcForConsole(src));
		pendingImagesCount++;
		log('pendingImagesCount: ' + pendingImagesCount);
		var vtabid = (virtualTabId++);
		var bounds = getBoundsFor(img);
		var screen = extend(window.screen);
		screen.availHeight = window.screen.availHeight;
		screen.availLeft = window.screen.availLeft;
		screen.availTop = window.screen.availTop;
		screen.availWidth = window.screen.availWidth;
		screen.colorDepth = window.screen.colorDepth;
		screen.height = window.screen.height;
		screen.orientation = {
			angle: !window.screen || !window.screen.orientation ? 0 : window.screen.orientation.angle,
			type: !window.screen || !window.screen.orientation ? 'unknown' : window.screen.orientation.type
		};
		screen.pixelDepth = window.screen.pixelDepth;
		screen.width = window.screen.width;
		screen.position = {
			x: window.screenX,
			y: window.screenY
		};
		var plugins = [];
		each(window.navigator.plugins, function (p) {
			plugins.push({
				name: p.name,
				description: p.description,
				filename: p.filename
			});
		});
		var params = {
			url: src,
			urls: img.altsrc,
			referer: window.location,
			referrer: document.referrer,
			alt: img.alt || img.title,
			navigator: {
				userAgent: window.navigator.userAgent,
				platform: window.navigator.platform,
				language: window.navigator.language,
				languages: window.navigator.languages,
				plugins: plugins,
				platform: window.navigator.platform,
				cookieEnabled: window.navigator.cookieEnabled,
				vendor: window.navigator.vendor,
				doNotTrack: window.navigator.doNotTrack
			},
			scroll: {
				left: window.scrollX,
				top: window.scrollY
			},
			offset: {
				x: bounds.left,
				y: bounds.top,
				width: bounds.width,
				height: bounds.height
			},
			ext: {
				chrome: manifest.version
			},
			click: infos.clickPos,
			title: document.title,
			screen: screen,
			strategy: infos.strategy,
			timer: infos.timer
		};
		isDev && pendingImagesCount === 1 && document.body.appendChild(loading);
		var changeTabUrl = function (computedUrl) {
			if (!tabs[vtabid]) {
				pendingRedirectionTabs[vtabid] = computedUrl;
				log('change tab pending set to ' + computedUrl);
				log('------------------------------');
			} else {
				chrome.runtime.sendMessage({
					target: 'background',
					command: 'change',
					url: computedUrl,
					active: tabs[vtabid].active,
					tabid: tabs[vtabid].tabid,
					instanceId: instanceId
				});
				log('tab changed to ' + computedUrl);
				log('------------------------------');
				delete tabs[vtabid];
			}
		};
		$.ajax({
			verb: 'POST',
			url: server + 'new',
			mime: 'application/json',
			payload: params,
			error: function () {
				var computedUrl = server + 'error/fatal';
				log('------------------------------');
				log('changing new tab to fatal error');
				changeTabUrl(computedUrl);
			},
			completed: function (xhr) {
				var data = $.ajax.json(xhr);
				if (!!data.error) {
					var errorCode = data.errorCode || data.error;
					var status = (!!~errorCode.indexOf('?') ? '&' : '?') + 's=' + xhr.status;
					var computedUrl = server + 'error/' + errorCode + status;
					log('------------------------------');
					log('changing new tab to error ' + errorCode);
					changeTabUrl(computedUrl);
				}
				else {
					var qs = [];
					if (!!data.token) {
						qs.push('t=' + data.token);
					}
					if (!qs.length) {
						qs = '';
					} else {
						qs = '?' + qs.join('&');
					}
					var computedUrl = server + data.hash + qs;
					log('------------------------------');
					log('changing new tab to ' + computedUrl);
					changeTabUrl(computedUrl);
				}
			},
			always: function (xhr) {
				pendingImagesCount--;
				isDev && pendingImagesCount === 0 && document.body.removeChild(loading);
			}
		});
		log(params);
		log(infos.strategy);
		log(infos.timer.delta + ' ms');
		log('pendingImagesCount: ' + pendingImagesCount);
		log('------------------------------');

		log('------------------------------');
		log('opening new tab');
		chrome.runtime.sendMessage({
			target: 'background',
			command: 'open',
			url: server + 'loading',
			active: !infos.clickPos.meta && !infos.clickPos.ctrl,
			vtabid: vtabid,
			instanceId: instanceId
		});
		log('new loading tab open request send');
		log('------------------------------');

		return true;
	};
	var hasPendingImages = function () {
		return pendingImagesCount > 0;
	};
	
	var hitTest = function (pos, collection) {
		var candidates = [];
		each(collection, function (elem) {
			if (posIsInElement(pos, elem)) {
				candidates.push(elem);
			}
		});
		return candidates;
	};
	
	var removeInvisibles = function (collection) {
		var candidates = [];
		each(collection, function (elem) {
			if (isVisible(elem)) {
				candidates.push(elem);
			}
		});
		return candidates;
	};

	var validateIsElement = function (elem, localName) {
		return elem !== window && elem !== document && elem.localName === localName;
	};
	
	var validateIsImg = function (elem) {
		return validateIsElement(elem, 'img') &&
			elem.width > 9 && elem.height > 9 &&
			elem.naturalWidth > 9 && elem.naturalHeight > 9;
	};

	var validateIsPicture = function (elem) {
		if (validateIsElement(elem, 'picture')) {
			var sources = all(elem, 'source');
			if (!!sources && !!sources.length) {
				sources = arrayize(sources);
				sources = sources.concat(arrayize(all(elem, 'img')));
				var url, urls;
				if (sources.length > 1) {
					var urlsAndSizes = extractAllSourcesAndSizes(sources); //extrait pr chaque la best source et sa size
					urls = map(urlsAndSizes, function (urlAndSize) {
						return urlAndSize[0];
					});
					url = chooseBestUrlFromDifferentSrcSet(urlsAndSizes);
				} else {
					url = urls = sources.srcset || sources.src;
				}
				
				var title = map(sources, function (source) {
					return source.alt || source.title;
				}).filter(identity).join(' - ');

				return {
					src: url, // random position
					altsrc: urls,
					alt: elem.title || title,
					bounds: elem.getBoundingClientRect(),
					elem: elem
				};
			}
		}
		return false;
	};

	var validateIsCanvas = function (elem) {
		if (validateIsElement(elem, 'canvas')) {
			var exportQuality = 1;
			var exportType = 'image/jpeg';
			var emptyC = document.createElement('canvas');
			emptyC.width = elem.width;
			emptyC.height = elem.height;
			var emptyUrl = emptyC.toDataURL(exportType);
			var src = elem.toDataURL(exportType);
			if (!src) {
				log('Failed to export canvas!');
			} else if (src !== emptyUrl) {
				return {
					src: elem.toDataURL(exportType, exportQuality),
					alt: elem.title,
					bounds: elem.getBoundingClientRect(),
					elem: elem
				};
			} else {
				log('Detected an empty canvas!');
			}
		}
		return false;
	};
	
	var urlRegExp = /^url\(["']?(.+?)["']?\)$/i;
	var imgSetRegExp = /^-webkit-image-set\(url\(["']?(.+?)["']?\)/i;
	var extractUrlFromSrc = function (style, bgImage) {
		if (!!style.backgroundImage && style.backgroundImage !== 'none') {
			if (urlRegExp.test(style.backgroundImage)) {
				bgImage = style.backgroundImage.replace(urlRegExp, '$1');
			} else if (imgSetRegExp.test(style.backgroundImage)) {
				bgImage = imgSetRegExp.exec(style.backgroundImage)[1];
				var test = imgSetRegExp.exec(style.backgroundImage)[2]
			} 	
			return bgImage ? bgImage : false;
		}
		return false;
	};

	var validateHasBgImage = function (elem) {
		if (elem === window || elem === document) {
			return false;
		}
		var style = getComputedStyleFor(elem);
		var bounds = elem.getBoundingClientRect();
		var bgImage = extractUrlFromSrc(style, bgImage);
	
		if (!!bgImage) {
			return {
				src: bgImage,
				alt: elem.title,
				bounds: bounds,
				elem: elem
			};
		}
		return false;
	};

	var validatePseudoElHasBgImage = function (elem) {
		if (elem === window || elem === document) {
			return false;
		}
		var style;
		var bounds = elem.getBoundingClientRect();
		var bgImage;

		var hasPseudoElWithBgImage =  function (elem, pseudoElement) {
			return window.getComputedStyle(elem, pseudoElement).backgroundImage != 'none';
		};

		var extractBgImageFromPseudoEl = function (pseudoElement) {
			if (hasPseudoElWithBgImage(elem, pseudoElement)) {
				style = window.getComputedStyle(elem, pseudoElement);
				bgImage = extractUrlFromSrc(style, bgImage);
			}
		};

		each([':after', ':before'], extractBgImageFromPseudoEl);

		if (!!bgImage) {
			return {
				src: bgImage,
				alt: elem.title,
				bounds: bounds,
				elem: elem
			};
		}
		return false;
	};

	
	var findAll = function (path, validator) {
		var all = [];
		each(path, function (elem, index) {
			if (!!elem) {
				var result = validator(elem, index, all);
				if (!!result) {
					all.push(result === true ? elem : result);
				}
			}
		});
		return all;
	};
	
	var findOne = function (path, validator) {
		return findAll(path, function (elem, index, all) {
			return all.length === 0 && validator(elem, index, all);
		})[0];
	};
	
	var findChildrenInPath = function (path, all) {
		var candidates = [];
		each(path, function (elem) {
			if (!candidates.length && elem !== document && elem !== window) {
				candidates = all(elem);
			}
		});
		return candidates;
	};

	var findElementsWithBackground = function (selector) {
		return function _findElementsWithBackground(elem) {
			return findAll(all(elem, selector), validateHasBgImage);
		};
	};

	var findPseudoElWithBackground = function (selector) {
		return function _findPseudoElWithBackground(elem) {
			return findAll(all(elem, selector), validatePseudoElHasBgImage);
		};
	};

	// https://gist.github.com/benpickles/4059636
	var allParents = function (elem) {
		var parents = [];
		var e = elem.parentElement;
		while (!!e) {
			parents.unshift(e);
			e = e.parentElement;
		}
		return parents;
	};

	var findCommonAncestor = function (a, b) {
		var elemA = a.elem || a;
		var elemB = b.elem || b;
		
		// same ancestor
		if (elemA.parentElement === elemB.parentElement) {
			return {
				a: a,
				b: b,
				common: elemA.parentElement
			};
		}
		
		var parentsA = allParents(elemA);
		var parentsB = allParents(elemB);
		
		if (parentsA[0] !== parentsB[0]) {
			throw new Error('No common ancestor!');
		}

		var aLength = parentsA.length;
		var bLength = parentsB.length;

		// start from html and go down. Stop when the chain is broken.
		for (var i = 0; i < Math.min(aLength, bLength); i++) {
			if (parentsA[i] !== parentsB[i]) {
				return {
					a: parentsA[i],
					b: parentsB[i],
					common: parentsA[i - 1]
				};
			}
		}
		// Both trees are equal (checking shallowest length)
		// so return last elements
		return {
			a: parentsA[i] || parentsA[i - 1],
			b: parentsB[i] || parentsB[i - 1],
			common: parentsA[i - 1]
		};
	};
	
	var findFromRenderStack = function (candidates) {
		var matrix = [];
		candidates.forEach(function (rowC, rowI) {
			var row = [];
			matrix.push(row);
			candidates.forEach(function (colC, colI) {
				row[colI] = rowI === colI ? NaN : 0;
			});
		});
		printMatrix(matrix);
		candidates.forEach(function (rowC, rowI) {
			candidates.forEach(function (colC, colI) {
				if (colI <= rowI || rowC === colC) {
					return;
				}
				var c = findCommonAncestor(rowC, colC);
				if (!c) {
					return;
				}
				var styleA = getComputedStyleFor(c.a);
				var styleB = getComputedStyleFor(c.b);
				var isPositioned = function (style) {
					return style.position !== 'static';
				};
				var positionedA = isPositioned(styleA);
				var positionedB = isPositioned(styleB);
				var setA = function () {
					matrix[rowI][colI] += 1;
				};
				var setB = function () {
					matrix[colI][rowI] += 1;
				};
				var normalStack = function () {
					var compare = c.a.compareDocumentPosition(c.b);
					if (compare === c.a.DOCUMENT_POSITION_PRECEDING) {
						// B precedes A
						setA();
					}
					else if (compare === c.a.DOCUMENT_POSITION_FOLLOWING) {
						// B follow A
						setB();
					}
					/*else if (compare === c.a.DOCUMENT_POSITION_CONTAINS) {
						// B contains A
						setA();
					}
					else if (compare === c.a.DOCUMENT_POSITION_CONTAINED_BY) {
						// B is contained by A
						setB();
					}*/
					else {
						throw new Error('Nodes must be siblings!');
					}
				};
				// https://developer.mozilla.org/en-US/docs/Web/Guide/CSS/Understanding_z_index/Adding_z-index
				if (positionedA && positionedB) {
					if (z(styleA.zIndex) === z(styleB.zIndex)) {
						// normal stacking
						normalStack();
					}
					else if (z(styleA.zIndex) > z(styleB.zIndex)) {
						// consider A on top
						setA();
					}
					else {
						// consider B on top
						setB();
					}
				}
				else if (!positionedA && positionedB) {
					// consider B on top
					setB();
				}
				else if (positionedA && !positionedB) {
					// consider A on top
					setA();
				}
				else {
					// normal stacking
					normalStack();
				}
			});
		});
		printMatrix(matrix);
		matrix.forEach(function (row, i) {
			var sum = 0;
			row.forEach(function (value) {
				sum += (value || 0);
			});
			matrix[i] = sum;
		});
		var max = 0, img;
		matrix.forEach(function (total, i) {
			if (max <= total) {
				max = total;
				img = candidates[i];
			}
		});
		return img;
	};

	var mustIgnoreEventKeys = function (e) {
		return !e.shiftKey || !!e.altKey;
	};

	var mustIgnoreEventTarget = function (e) {
		if (!!canvas.visible) {
			return true;
		}
		if (!e || !e.target) {
			return true;
		}
		var localName = e.target.localName;
		var contentEditable = e.target.getAttribute('contenteditable');
		return (!localName ||
			localName === 'input' || localName === 'textarea' ||
			localName === 'object' || localName === 'iframe' ||
			localName === 'select' ||
			contentEditable === '' || contentEditable === 'true'
		);
	};
	
	var copyEvent = function (e) {
		return {
			pageX: e.pageX,
			pageY: e.pageY,
			shiftKey: e.shiftKey,
			metaKey: e.metaKey,
			altKey: e.altKey,
			ctrlKey: e.ctrlKey,
			path: e.path,
			target: e.target
		};
	};
	
	var click = function (e, bypass) {
		log('click');
		var start = now();
		var pos = {
			x: e.pageX,
			y: e.pageY,
			shift: e.shiftKey,
			meta: e.metaKey,
			ctrl: e.ctrlKey,
			alt: e.altKey
		};
		var img;
		var found = function (strategy, group) {
			if (!img) {
				throw new Error('img cannot be falsy');
			}
			var end = now();
			log("Candidate: ");
			imageFound(img, {
				strategy: (!!group ? group + '-' : '') + strategy,
				clickPos: pos,
				timer: {
					start: start,
					end: end,
					delta: end - start
				}
			});
			preventDefault(e);
			return true;
		};

		// ignore ?
		if (bypass !== true && mustIgnoreEventKeys(e)) {
			return true;
		}
		if (mustIgnoreEventTarget(e)) {
			return true;
		}
		
		// create a style cache
		initStyleCache();
		
		var findIt = function (options) {
			// easiest routes (most often wrong...)
			img = findOne([e.target], options.validator);
			if (!!img) {
				return found('eventTarget', options.group);
			}
			img = document.elementFromPoint(pos.x, pos.y);
			img = findOne([img], options.validator);
			if (!!img) {
				return found('elementFromPoint', options.group);
			}
			
			// try to find a something in the event path
			img = findOne(e.path, options.validator);
			if (!!img) {
				return found('findInPath', options.group);
			}
			
			// if not found, find images in children in event path
			var candidates = findAll(findChildrenInPath(e.path, options.all), options.validator);
			candidates = removeInvisibles(candidates);
			candidates = hitTest(pos, candidates);
			if (candidates.length === 1) {
				img = candidates[0];
				return found('findChildrenInPath', options.group);
			}
			else if (candidates.length > 0) {
				img = findFromRenderStack(candidates);
				if (!!img) {
					return found('findChildrenInPath+findFromRenderStack', options.group);
				}
			}
			
			// if still not found in event path
			// we need to check the WHOLE document
			// for possible hit candidates.
			candidates = findAll(options.all(), options.validator);
			candidates = removeInvisibles(candidates);
			candidates = hitTest(pos, candidates);
			if (candidates.length === 1) {
				img = candidates[0];
				return found('querySelectorAll', options.group);
			}
			else if (candidates.length > 0) {
				img = findFromRenderStack(candidates);
				if (!!img) {
					return found('querySelectorAll+findFromRenderStack', options.group);
				}
			}
			return false;
		};

		// Try with all <picture />'s
		var options = {
			validator: validateIsPicture,
			group: 'picture',
			all: function (elem) {
				return all(elem, 'picture');
			}
		};
		if (findIt(options)) {
			return false;
		}
		
		// Try with all <img />'s
		options = {
			validator: validateIsImg,
			group: 'img',
			all: function (elem) {
				return all(elem, 'img');
			}
		};
		if (findIt(options)) {
			return false;
		}

		// try with <canvas />
		options = {
			validator: validateIsCanvas,
			group: 'canvas',
			all: function (elem) {
				return findAll(all(elem, 'canvas'), validateIsCanvas);
			}
		};
		if (findIt(options)) {
			return false;
		}

		// Do the same, but with all elements under html
		// that have a background image
		options = {
			validator: validateHasBgImage,
			group: 'bg',
			all: findElementsWithBackground('html *')
		};
		if (findIt(options)) {
			return false;
		}

		// Do the same, but with all pseudo elements under html
		// that have a background image
		options = {
			validator: validatePseudoElHasBgImage,
			group: 'bg',
			all: findPseudoElWithBackground('html *')
		};
		if (findIt(options)) {
			return false;
		}
		
		// try html node
		// keep it separate because it does not have any parent
		options = {
			validator: validateHasBgImage,
			group: 'html',
			all: findElementsWithBackground('html')
		};
		if (findIt(options)) {
			return false;
		}
		
		log('no Image found');
		log(pos);
		return true;
	};

	var lastKeyDownEvent = {};
	var lastKeyDown = function (e) {
		lastKeyDownEvent = copyEvent(e);
	};

	var paste = function (e) {
		var pasteStart = now();
		var lastCopy = copyEvent(lastKeyDownEvent);
		var processImageItem = function (item) {
			var file = item.getAsFile();
			var reader = new FileReader();
			var read = function () {
				var end = now();
				imageFound({
					title: document.title,
					src: reader.result,
					bounds: {
						top: window.scrollY,
						right: window.scrollX,
						bottom: window.innerHeight + window.scrollY,
						left: window.innerWidth + window.scrollX,
						width: window.innerWidth,
						height: window.innerHeight
					}
				}, {
					strategy: 'paste',
					clickPos: {
						x: lastCopy.pageX,
						y: lastCopy.pageY,
						shift: lastCopy.shiftKey,
						meta: lastCopy.metaKey,
						ctrl: lastCopy.ctrlKey,
						alt: lastCopy.altKey
					},
					timer: {
						start: pasteStart,
						end: end,
						delta: end - pasteStart
					}
				});
			};
			reader.addEventListener('load', read, false);
			reader.readAsDataURL(file);
		};
		each(e.clipboardData.items, function (item) {
			if (!!~item.type.indexOf('image')) {
				processImageItem(item);
			}
		});
	};

	var dragenter = function (e) {
		//console.log('dragenter');
		e.target.style.opacity = '0.4';
	};

	var dragover = function (e) {
		//console.log('dragover');
		e.dataTransfer.dropEffect = 'copy';
		return preventDefault(e);
	};

	var dragleave = function (e) {
		e.target.style.opacity = '';
		//console.log('dragleave');
	};

	var drop = function (e) {
		e.target.style.opacity = '';
		var files = e.dataTransfer.files;
		log('Dragged ' + files.length + ' files');
		
		var dropStart = now();
		var lastCopy = {}; // copyEvent(lastKeyDownEvent);
		var processImageItem = function (file) {
			var reader = new FileReader();
			var read = function () {
				var end = now();
				imageFound({
					title: document.title,
					src: reader.result,
					bounds: {
						top: window.scrollY,
						right: window.scrollX,
						bottom: window.innerHeight + window.scrollY,
						left: window.innerWidth + window.scrollX,
						width: window.innerWidth,
						height: window.innerHeight
					}
				}, {
					strategy: 'paste',
					clickPos: {
						x: lastCopy.pageX,
						y: lastCopy.pageY,
						shift: lastCopy.shiftKey,
						meta: lastCopy.metaKey,
						ctrl: lastCopy.ctrlKey,
						alt: lastCopy.altKey
					},
					timer: {
						start: dropStart,
						end: end,
						delta: end - dropStart
					}
				});
			};
			reader.addEventListener('load', read, false);
			reader.readAsDataURL(file);
		};
		
		for (var i = 0; i < files.length; i++) {
			var file = files[i];
			var imageType = /^image\//;
			
			if (!imageType.test(file.type)) {
				continue;
			}
			
			processImageItem(file);
		}
		return preventDefault(e);
	};

	var printMatrix = function (matrix) {
		log('--------');
		matrix.forEach(function (rowC, rowI) {
			var s = '';
			rowC.forEach(function (colC, colI) {
				s += isNaN(colC) ? '+' : colC;
				s += ' ';
			});
			log(s);
		});
		log('--------');
	};
	
	var preventDefault = function (e) {
		if (e.preventDefault) {
			e.preventDefault();
			e.stopPropagation();
			e.stopImmediatePropagation();
		}
		return false;
	};

	var noop = function (e) {
		// ignore ?
		if (mustIgnoreEventKeys(e) || mustIgnoreEventTarget(e)) {
			return true;
		}
		if (!hasPendingImages()) {
			return true;
		}
		return preventDefault(e);
	};

	let selectionStart = now();
	const selectionHotKey = 16; // shift
	const selectionAbortKey = 27; // escape
	const selectionRectOpts = {
		opacity: 0.2
	};
	const drawSelection = (x, y) => {
		canvas.clear();
		canvas.update(x, y);
		canvas.rect(selectionRectOpts);
	};
	const initSelection = (e) => {
		log('selection mode on');
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
		canvas.elem.width = canvas.width;
		canvas.elem.height = canvas.height;
		canvas.clear();
		document.body.appendChild(canvas.elem);
		canvas.visible = true;
		canvas.mousedown = false;
		canvas.selection = false;
		selectionStart = now();
	};
	const teardownSelection = () => {
		log('selection mode off');
		document.body.removeChild(canvas.elem);
		canvas.y = 0;
		canvas.x = 0;
		canvas.visible = false;
		canvas.selection = false;
		canvas.mousedown = false;
	};
	const sendViewportSelection = function (e, strategy) {
		var end = now();

		var pixelRatio = window.devicePixelRatio;

		var width = window.innerWidth * pixelRatio;
		var height = window.innerHeight * pixelRatio;
		var msg = {
			target: 'background',
			command: 'screenshot',
			instanceId: instanceId,
			params:{
				image: {
					title: document.title,
					pos: {
						x1: 0,
						x2: width,
						y1: 0,
						y2: height
					},
					bounds: getBoundsFor(canvas.elem)
				},
				infos: {
					strategy: strategy,
					clickPos: {
						x: 0,
						y: 0,
						shift: null,
						meta: null,
						ctrl: null,
						alt: null
					},
					timer: {
						start: null,
						end: null,
						delta: null
					}
				}
			}
		};
		setTimeout(function () {
			chrome.runtime.sendMessage(msg);
		}, 16); // DOM refresh delay
	};
	const sendSelection = function (e, strategy) {
		var end = now();
		var msg = {
			target: 'background',
			command: 'screenshot',
			instanceId: instanceId,
			params:{
				image: {
					title: document.title,
					pos: canvas.pos(),
					bounds: getBoundsFor(canvas.elem)
				},
				infos: {
					strategy: strategy,
					clickPos: {
						x: canvas.x,
						y: canvas.y,
						shift: !!(e && e.shiftKey),
						meta: !!(e && e.metaKey),
						ctrl: !!(e && e.ctrlKey),
						alt: !!(e && e.altKey)
					},
					timer: {
						start: selectionStart,
						end: end,
						delta: end - selectionStart
					}
				}
			}
		};
		setTimeout(function () {
			chrome.runtime.sendMessage(msg);
		}, 16); // DOM refresh delay
	};
	let captureTimer = 0;
	const captureRectOpts = {
		opacity: 0.4
	};
	const captureDelay = 600;
	const captureSelection = (e, strategy) => {
		canvas.clear();
		canvas.rect(captureRectOpts);
		clearTimeout(captureTimer);
		captureTimer = setTimeout(function () {
			if (!canvas.mousedown && canvas.selection) {
				teardownSelection();
				sendSelection(e, strategy);
			}
			captureTimer = 0;
		}, captureDelay);
	};
	let keydownLastHotKey = 0;
	let keyupLastEvent = 0;
	let keyupHotKeyCount = 0;
	let keyupSelection = null;
	const keydownHotKeyTimeLimit = 100;
	const keyupHotKeyTarget = 3;
	const keyupHotKeyTimeLimit = 2000;
	const keyupSelectionBounds = (pad) => {
		pad = pad || 0;
		const node = (elem) => {
			if (elem.nodeType === HTMLDocument.TEXT_NODE) {
				elem = elem.parentNode;
			}
			return elem;
		};
		const a = node(keyupSelection.anchorNode).getBoundingClientRect();
		const f = node(keyupSelection.focusOffset === 0 && keyupSelection.focusNode.previousSibling ?
			keyupSelection.focusNode.previousSibling :
			keyupSelection.focusNode
		).getBoundingClientRect();
		const b = {
			x1: Math.min(f.left, a.left),
			y1: Math.min(f.top, a.top),
			x2: Math.max(f.right, a.right),
			y2: Math.max(f.bottom, a.bottom)
		};
		if (pad !== 0) {
			b.x1 = Math.max(0, b.x1 - pad);
			b.y1 = Math.max(0, b.y1 - pad);
			b.x2 = Math.min(window.innerWidth, b.x2 + pad);
			b.y2 = Math.min(window.innerHeight, b.y2 + pad);
		}
		return b;
	};
	var keydown = function (e) {
		if (e.which === selectionAbortKey) {
			if (!!canvas.visible) {
				if (canvas.selection) {
					canvas.clear();
					canvas.selection = false;
					canvas.mousedown = false;
				}
				else {
					teardownSelection();
				}
				return preventDefault(e);
			}
		} else if (e.which === selectionHotKey) {
			log('shift');
			keydownLastHotKey = now();
		}
	};
	var keyup = function (e) {
		if (!!canvas.visible) {
			return;
		}
		if (e.which === selectionHotKey) {
			const n = now();
			if (n - keydownLastHotKey > keydownHotKeyTimeLimit) {
				keyupSelection = null;
				keyupHotKeyCount = 0;
				return;
			} else if (n - keyupLastEvent > keyupHotKeyTimeLimit) {
				if (keyupHotKeyCount > 0) {
					log('Selection time limit, bail out.');
				}
				keyupSelection = null;
				keyupHotKeyCount = 0;
			}
			var newSelection = window.getSelection();
			if (keyupHotKeyCount === 0) {
				keyupLastEvent = now();
				keyupSelection = {
					isCollapsed: newSelection.isCollapsed,
					anchorNode: newSelection.anchorNode,
					focusNode: newSelection.focusNode,
					anchorOffset: newSelection.anchorOffset,
					focusOffset: newSelection.focusOffset,
					type: newSelection.type,
					rangeCount: newSelection.rangeCount
				};
			}
			else if (newSelection.isCollapsed !== keyupSelection.isCollapsed ||
					newSelection.anchorNode !== keyupSelection.anchorNode ||
					newSelection.focusNode !== keyupSelection.focusNode ||
					newSelection.anchorOffset !== keyupSelection.anchorOffset ||
					newSelection.focusOffset !== keyupSelection.focusOffset ||
					newSelection.type !== keyupSelection.type ||
					newSelection.rangeCount !== keyupSelection.rangeCount
				) {
				log('Selection range change, bail out.');
				keyupSelection = null;
				keyupHotKeyCount = 0;
				return;
			}
			keyupHotKeyCount++;
			if (keyupHotKeyCount >= keyupHotKeyTarget) {
				initSelection(e);
				if (!!keyupSelection && !keyupSelection.isCollapsed) {
					log('Found valid text selection');
					var b = keyupSelectionBounds(8);
					canvas.selection = true;
					canvas.x = b.x1;
					canvas.y = b.y1;
					drawSelection(b.x2, b.y2);
					captureSelection(e, 'selection');
				}
				keyupSelection = null;
				keyupHotKeyCount = 0;
			}
		}
		else {
			keyupHotKeyCount = 0;
			keyupSelection = null;
		}
	};
	var mousemove = function (e) {
		if (!!canvas.mousedown) {
			canvas.selection = true;
			drawSelection(e.x, e.y);
			return preventDefault(e);
		}
	};
	var mouseup = function (e) {
		if (!!canvas.mousedown) {
			if (!!canvas.selection) {
				captureSelection(e, 'screenshot');
			}
			canvas.mousedown = false;
			return preventDefault(e);
		}
	};
	var mousedown = function (e) {
		if (!!canvas.visible) {
			if (!captureTimer) {
				canvas.clear();
				canvas.selection = false;
				canvas.mousedown = true;
				canvas.y = e.y;
				canvas.x = e.x;
			}
			return preventDefault(e);
		}
	};

	var cropToSelection = function (src, pos, cb) {

		if (!src) {
			log('Cannot crop empty source!');
			return;
		}
		var c = document.createElement('canvas');
		var pixelRatio = window.devicePixelRatio;

		c.width = (pos.x2 - pos.x1) * pixelRatio || 0;
		c.height = (pos.y2 - pos.y1) * pixelRatio;	

		if (c.width === 0 || c.height === 0) {
			log('Cannot create a ' + c.width + 'x' + c.height + ' image');
		} else {
			var img = new Image();
			img.onload = function () {
				var x = -pos.x1 * pixelRatio;
				var y = -pos.y1 * pixelRatio;
				c.getContext('2d').drawImage(img, x, y);
				var xport = validateIsCanvas(c);
				cb(xport.src);
			};
			img.src = src;
		}
	};
	
	var hostname = document.location.hostname;
	var localhost = hostname === 'localhost' || hostname === '127.0.0.1';
	var shftcl = hostname === 'shft.cl';
	var shftclDev = hostname === 'dev.shft.cl';
	var lastContextMenuEvent = null;
	var commands = {
		click: function () {
			if (!lastContextMenuEvent) {
				log('right click ignored: no event found.');
				return;
			}
			click(lastContextMenuEvent, true);
			lastContextMenuEvent = null;
		},
		screenshot: function (params) {
			if (window.parent !== window) {
				log('Screenshot can only be taken from the top most window');
				return;
			}
			params.image.elem = canvas.elem;
			cropToSelection(params.image.src, params.image.pos, function (src) {
				if (!src) {
					log('screenshot failed :(');
				} else {
					params.image.src = src;
					imageFound(params.image, params.infos);
				}
			});
		},
		captureViewport: function (e) {
			sendViewportSelection(e, "Full viewport screenshot");
		},
		open: function (params) {
			if (pendingCloseTabs[params.vtabid]) {
				chrome.runtime.sendMessage({
					target: 'background',
					command: 'close',
					tabid: params.tabid,
					instanceId: instanceId
				});
				delete pendingCloseTabs[params.vtabid];
			} else if (pendingRedirectionTabs[params.vtabid]) {
				chrome.runtime.sendMessage({
					target: 'background',
					command: 'change',
					url: pendingRedirectionTabs[params.vtabid],
					active: params.active,
					tabid: params.tabid,
					instanceId: instanceId
				});
				delete pendingRedirectionTabs[params.vtabid];
			} else {
				tabs[params.vtabid] = {
					tabid: params.tabid,
					active: params.active
				};
			}
		}
	};

	// enable with chrome.storage.local.set({'devMode': true})
	storage.get('devMode', function (items) {
		if (!!items.devMode) {
			var devMode = items.devMode === true ? 'dev' : items.devMode;
			server = 'https://' + devMode + '.shft' + '.cl' + '/';
		}
	});

	chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
		if (!!request.command) {
			if (typeof request.instanceId !== 'undefined' && request.instanceId !== instanceId) {
				log('Ignored message destinated to ' + request.instanceId);
				return;
			}
			var res;
			if (!!commands[request.command]) {
				res = commands[request.command](request.params, sender);
			}
			else {
				log('Command ' + request.command + ' ignored.');
			}
			//sendResponse(res);
		}
	});

	var startPing = function () {
		if (window.navigator.doNotTrack) {
			log('Ping ignored for doNotTrack users');
			return;
		}
		var pingDelay = (~~(Math.random() * 100000) % 5000) + 2000;
		if (!(localhost && isDev)) {
			setTimeout(ping, pingDelay);
		}
		log('user ' + user.id + ' found. Init with delay ' + pingDelay);
	};
	
	storage.get('user', function (items) {
		if (items.user && items.user.id) {
			user = items.user;
			startPing();
			return;
		}
		user = {
			id: (function () {
				var randomPool = new Uint8Array(32);
				crypto.getRandomValues(randomPool);
				var hex = '';
				for (var i = 0; i < randomPool.length; ++i) {
					hex += randomPool[i].toString(16);
				}
				return hex;
			})()
		};
		log('creating new user...');
		storage.set({ user: user }, function () {
			if (chrome.runtime.lastError) {
				log('error while saving user: ' + chrome.runtime.lastError);
			} else {
				startPing();
			}
		});
	});

	if ((localhost && isDev) || shftcl || shftclDev) {
		if (shftcl || shftclDev) {
			$('.js-chrome-ext', $.remove);
		}
		$('.js-chrome-ext-paste', function (elem) {
			all(elem, '.js-chrome-ext-paste-target', function (elem) {
				elem.classList.remove('hidden');
				elem.addEventListener('dragenter', dragenter, false);
				elem.addEventListener('dragover', dragover, false);
				elem.addEventListener('dragleave', dragleave, false);
				elem.addEventListener('drop', drop, false);
			});
			all(elem, '.js-chrome-ext-paste-text', function (elem) {
				elem.classList.remove('hidden');
			});
			document.addEventListener('paste', paste, true);
			log('Paste and drag registered');
		});
		$('.js-chrome-ext-buckets', function (elem) {
			var bucketText = function (bucket) {
				return bucket.bucket + (
					!bucket.count ? '' : ' (' + bucket.count + ')'
				);
			};
			var bucketLink = function (bucket) {
				var link = document.createElement('a');
				link.innerHTML = bucketText(bucket);
				link.setAttribute('data-hash', bucket.hash);
				link.setAttribute('href', '/bucket/' + bucket.hash);
				link.classList.add(bucket.cssClass || 'bucket-link');
				return link;
			};
			var hash;
			$('[data-hash]', function (c) {
				hash = c.getAttribute('data-hash');
			});
			elem.classList.remove('hidden');
			all(elem, 'input', function (input) {
				input.addEventListener('keyup', function (e) {
					if (e.which === 13 && input.value) {
						$.ajax({
							verb: 'POST',
							payload: {
								user: user,
								bucket: input.value,
								hash: hash
							},
							url: server + 'buckets/new',
							mime: 'application/json',
							completed: function (xhr) {
								var data = $.ajax.json(xhr);
								if (!!data.error || !data.bucket) {
									log('Could not add to bucket');
								} else {
									var found = false;
									var linkSel = '.js-chrome-ext-buckets-image a[data-hash="' + data.bucket.hash + '"]';
									all(elem, linkSel, function (elem) {
										found = true;
									});
									found || all(elem, '.js-chrome-ext-buckets-image', function (elem) {
										var l = bucketLink(data.bucket);
										elem.appendChild(l);
									});
								}
							}
						});
						input.value = '';
						return preventDefault(e);
					}
				}, false);
			});
			setTimeout(function () {
				$.ajax({
					verb: 'POST',
					payload: {
						user: user,
						hash: hash
					},
					url: server + 'buckets/my',
					mime: 'application/json',
					completed: function (xhr) {
						var data = $.ajax.json(xhr);
						if (!!data.error) {
							log('Could not load remote buckets.');
						} else if (data.buckets) {
							all(elem, '.js-chrome-ext-buckets-list', function (elem) {
								var node = elem.getAttribute('data-child-node');
								var link = elem.getAttribute('data-child-link');
								elem.innerHTML = '';
								each(data.buckets, function (bucket) {
									if (!!bucket.count) {
										return;
									}
									var n = document.createElement(node);
									if (!link) {
										n.setAttribute('value', bucket.bucket);
									} else {
										var l = bucketLink(bucket);
										n.appendChild(l);
									}
									elem.appendChild(n);
								});
							});
							all(elem, '.js-chrome-ext-buckets-image', function (elem) {
								each(data.buckets, function (bucket) {
									if (!!bucket.count) {
										var l = bucketLink(bucket);
										elem.appendChild(l);
									}
								});
							});
						} else {
							log('No remote buckets found');
						}
					}
				});
			}, 500);
		});
		return;
	}

	var mouseBL = [
		/^https?:\/\/docs\.google\.com\//i,
		/^https?:\/\/.*invisionapp\.com\//i,
		/^https?:\/\/app\.uxpin\.com\//i,
		/^https?:\/\/.*figma\.com\//i,
		/^https?:\/\/.*gmail\.com\//i,
		/^https?:\/\/mail\.google\.com\//i,
	];
	var isMouseBLed = function () {
		var r = false;
		mouseBL.forEach(function (b, i) {
			if (r) {
				return;
			}
			r = b.test(window.location.href);
		});
		return r;
	};
	if (!isMouseBLed()) {
		document.addEventListener('mousedown', click, true);
		document.addEventListener('mouseup', noop, true);
		document.addEventListener('click', noop, true);
		if (isDev) {
			document.addEventListener('paste', paste, true);
		}
		document.addEventListener('keydown', lastKeyDown, true);
	} else {
		log('Mouse BL.');
	}

	window.addEventListener('contextmenu', function (e) {
		lastContextMenuEvent = copyEvent(e);
	});

	if (window.parent !== window) {
		log('Canvas disabled in child windows.');
	} else if (canvas.supported) {
		document.addEventListener('keydown', keydown, true);
		document.addEventListener('keyup', keyup, true);
		canvas.elem.addEventListener('mousemove', mousemove, true);
		canvas.elem.addEventListener('mousedown', mousedown, true);
		canvas.elem.addEventListener('mouseup', mouseup, true);
	} else {
		log('Canvas unsupported.');
	}
	
	log('ext version ' + manifest.version);
	log('registered for ' + window.location);
})();
